﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_23._3
{
    internal class Werknemer
    {
        private string _naam;
        private string _voornaam;
        private double _verdiensten;

        public Werknemer() 
        { 

        }
        public Werknemer(string naam, string voornaam, double verdiensten)
        {
            Naam = naam;
            Voornaam = voornaam;
            Verdiensten = verdiensten;
        }
        public string Naam 
        { 
            get { return _naam; } 
            set 
            {
                if (value != "" && value != null)
                {
                    _naam = value;
                }
                else
                {
                    throw new Exception("De achternaam werd niet ingevuld!");
                }
            }
        }
        public string Voornaam
        {
            get { return _voornaam; }
            set
            {
                if (value != "" && value != null)
                {
                    _voornaam = value;
                }
                else
                {
                    throw new Exception("De voornaam werd niet ingevuld!");
                }
            }
        }
        public double Verdiensten
        {
            get { return _verdiensten; }
            set { _verdiensten = value; }
        }
        public string VolledigeWeergave
        {
            get { return Naam.PadRight(40) + Voornaam.PadRight(40) + Verdiensten + " \u20AC"; }
        }
    }
}
